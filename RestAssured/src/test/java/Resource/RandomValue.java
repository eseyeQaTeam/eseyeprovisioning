package Resource;

import java.util.Collections;
import java.util.Stack;

public class RandomValue {

    private int start;
    private int end;
    private Stack<Integer> numbers = new Stack<Integer>();
    public RandomValue(int start, int end){
        this.start = start;
        this.end = end;
    }
    private void loadNumbers(){
         for (int i=start;i<=end;i++){
                numbers.push(i);
            }
         Collections.shuffle(numbers);
    }
    public int nextInt(){
        if (numbers.empty()) loadNumbers();
        return numbers.pop();
    }
}