package Resource;

import java.text.SimpleDateFormat;
import java.util.Date;

import myreports.AutomationReports;
import myreports.AutomationTest;
import myreports.LogStatus;

public class Log {
	static AutomationReports reports;
	static AutomationTest testCase;

	public static void configure(String sFilePath, String sFileName) {
		reports = new AutomationReports(sFilePath, sFileName);
		System.out.println("HTML report path set at -> " + sFilePath
				+ sFileName);
	}

	public static void startSuite() {
		Log.info("******************** Starting Suite at "
				+ new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
						.format(new Date())
				+ " ************************************************");
	}

	public static void endSuite() {
		Log.info("******************** Ending Suite at "
				+ new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
						.format(new Date())
				+ " ************************************************");
	}

	public static void startTestCase(String sTestCaseName) {
		testCase = reports.startTest(sTestCaseName, sTestCaseName);

		Log.info("******************************************************************");

		Log.info("$$$     "
				+ sTestCaseName
				+ "  @  "
				+ new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
						.format(new Date()) + "  $$$");

		Log.info("******************************************************************");

	}

	// This is to print log for the ending of the test case

	public static void endTestCase(String sTestCaseName) {

		Log.info("XXXXXXXXXXXXXXXXXXXX " + "-E---N---D-  " + sTestCaseName
				+ "         XXXXXXXXXXXXX");
		reports.endTest(testCase);
		reports.flush();
	}

	// Need to create these methods, so that they can be called

	public static void info(String message) {

		testCase.log(LogStatus.INFO, message);

	}

	public static void error(String message) {

		testCase.log(LogStatus.ERROR, message);

	}

	public static void warn(String message) {

		testCase.log(LogStatus.WARNING, message);

	}

	public static void fail(String message) {

		testCase.log(LogStatus.FAIL, message);

	}

	public static void fatal(String message) {

		testCase.log(LogStatus.FATAL, message);

	}

	public static void pass(String message) {

		testCase.log(LogStatus.PASS, message);

	}

}// End of Class

