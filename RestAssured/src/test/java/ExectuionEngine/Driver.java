package ExectuionEngine;

import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Resource.Log;
import Resource.connectingToMySQLDB;

public class Driver extends pageElements {

	public static String ProxyAddress = "192.168.104.14", 
//			sOperator ="flo",
			sImsi,
			sOperator = "cncto",
			floQueue = "insertmq.php?queue=Provisioning.374130", sBaseURI = "http://stgprv01.staging.eseye.net/",
			canBounceRequest = sOperator + "/api/canBounce", canProvideRequest = sOperator + "/api/canProvide",
			canCeaseRequest = sOperator + "/api/canCease", canSuspendRequest = sOperator + "/api/canSuspend",
			canRestoreRequest = sOperator + "/api/canRestore", canreconcileRequest = sOperator + "/api/canReconcile";
	public static String sProvideRequest = sOperator + "/api/provideSubscription",
			sBounceRequest = sOperator + "/api/bounceSubscription", sCeaseRequest = sOperator + "/api/ceaseSubscription",
			sSuspendRequest = sOperator + "/api/suspendSubscription",
			sRestoreRequest = sOperator + "/api/restoreSubscription",
			sReconcileRequest = sOperator + "/api/reconcileSubscription";
	public static int ProxyPort = 3128;

	@BeforeTest
	public void initialConfiguration(ITestContext context) throws Exception {
		String sFilePath = context.getCurrentXmlTest().getParameter("selenium.htmlReportPath");

		String sFileName = context.getCurrentXmlTest().getParameter("htmlReport.test");
		Log.configure(sFilePath, sFileName);

		sImsi = context.getCurrentXmlTest().getParameter("floImsi");

		Log.startTestCase("Provisioning Automation Suite");
		

	}

	@Test(priority = 3)
	public void mQueueExecution() {
//		Log.info("Verifying operator name by imsi");
//		if (sImsi.startsWith("374130")) {
//			sOperator = "flo";

			// Execute case for MQueue Execution
			ApiRequest.fQueueExecution("provide", sImsi);
			ApiRequest.fQueueExecution("suspend", sImsi);
			ApiRequest.fQueueExecution("restore", sImsi);
			ApiRequest.fQueueExecution("cease", sImsi);
			ApiRequest.fQueueExecution("provide", sImsi);
		}
//	}

	@Test(priority = 2)
	public void canReqestExecution() {
		ApiRequest.fCanRequests("canSuspend", sImsi);
		ApiRequest.fCanRequests("canProvide", sImsi);
		ApiRequest.fCanRequests("canCease", sImsi);
		ApiRequest.fCanRequests("canRestore", sImsi);
		ApiRequest.fCanRequests("canBounce", sImsi);

	}

	@Test(priority = 1)
	public void provisionReqestExecution() {
		ApiRequest.fProvRequests("provide", sImsi);
		ApiRequest.fProvRequests("suspend", sImsi);
		ApiRequest.fProvRequests("provide", sImsi);
		ApiRequest.fProvRequests("cease", sImsi);
		ApiRequest.fProvRequests("restore", sImsi);
		ApiRequest.fProvRequests("bounce", sImsi);

	}
	 @Test(priority=4)
	 public void DataBaseConnection() {
	 try {
	 
	 String sQueryResult=connectingToMySQLDB.fGetStatus();
	 Log.info("Status of Apis are: "+ sQueryResult);
	 } catch (Exception e) {
	 Log.warn(e.toString());
	 }
	 }

	@AfterTest
	public void afterTest() {
		Log.info("This is the final status of requests: " + connectingToMySQLDB.sStatus);
		Log.endTestCase("Provisioning Automation Suite");
	}

}
