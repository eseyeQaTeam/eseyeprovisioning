package ExectuionEngine;

import java.util.HashMap;
import java.util.Map;

import Resource.Log;
import ExectuionEngine.ApiRequest;

public class pageElements {
	public static Map<String, String> map = new HashMap<String, String>();
	
	
	
	public static String getMQueuePayload(String sCan, String imsi) {
			
		map.put("requestID", ApiRequest.sTimeStamp.toString());
		map.put("requestType", sCan);
		map.put("ICCID", "89445385230012345");
		map.put("IMEI", "12345678909876565434");
		map.put("MEID", "A10000001234568");
		map.put("IMSIs", "[\r\n" + 
				"        {\r\n" + 
				"            \"IMSI\": \""+imsi+"\",\r\n" + 
				"            \"ICCID\": \"89445385230012345\",\r\n" + 
				"            \"providerTariff\": \"2GB_ESEYE\",\r\n" + 
				"            \"DATA\": \"MO,MT\",\r\n" + 
				"            \"SMS\": \"MO,MT\",\r\n" + 
				"            \"VOICE\": \"MO,MT\",\r\n" + 
				"            \"USSD\": \"MO,MT\",\r\n" + 
				"            \"OTA\": \"MO,MT\",\r\n" + 
				"            \"MSISDNs\": [\r\n" + 
				"                {\r\n" + 
				"                    \"MSISDN\": \"999130600000005\",\r\n" + 
				"                    \"type\": \"prim\"\r\n" + 
				"                }\r\n" + 
				"            ]\r\n" + 
				"        }\r\n" + 
				"    ]");
		map.put("APNs", "[\r\n" + 
				"        {\r\n" + 
				"            \"APN\": \"eseye.com\"\r\n" + 
				"        }\r\n" + 
				"    ]");
		map.put("replyTo", "Provising.FloMQueue");
			
		
		
		Log.info(map.toString());
				return map.toString();
	}
	
	public static String getCanRequestPayload(String sCan, String imsi) {
		String sData = "{\r\n" + 
				"	\"username\":\"qa\",\r\n" + 
				"	\"password\":\"Password@1\",\r\n" + 
				"	\"requestID\":\""+ApiRequest.sTimeStamp+"\",\r\n" + 
				"	\"requestType\":\""+sCan+"\",\r\n" + 
				"	\"ICCID\":\"89445385230012345\",\r\n" + 
				"	\"IMEI\":\"12345678909876565434\",\r\n" + 
				"	\"MEID\":\"A10000001234568\",\r\n" + 
				"	\"IMSIs\":\r\n" + 
				"	[\r\n" + 
				"	 {\r\n" + 
				"		\"IMSI\":\""+imsi+"\",\r\n" + 
				"		\"ICCID\":\"89445385230012345\",\r\n" + 
				"		\"providerTariff\":\"2GB_ESEYE\",\r\n" + 
				"		\"DATA\":\"MO,MT\",\r\n" + 
				"		\"SMS\":\"MO,MT\",\r\n" + 
				"		\"VOICE\":\"MO,MT\",\r\n" + 
				"		\"USSD\":\"MO,MT\",\r\n" + 
				"		\"OTA\":\"MO,MT\",\r\n" + 
				"		\"MSISDNs\":\r\n" + 
				"		[\r\n" + 
				"			{\"MSISDN\":\"999130600000005\",\r\n" + 
				"			\"type\":\"prim\"}\r\n" + 
				"		]\r\n" + 
				"		 }\r\n" + 
				"	],\r\n" + 
				"	 \r\n" + 
				"	\"APNs\":[\r\n" + 
				"			{\"APN\":\"eseye.com\"}\r\n" + 
				"		   ]\r\n" + 
				"	\r\n" + 
				"}";
		return sData;
	}

	
	
}
