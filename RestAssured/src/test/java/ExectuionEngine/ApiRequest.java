package ExectuionEngine;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.notNullValue;

import java.sql.Timestamp;
import java.util.Date;

import Resource.Log;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ApiRequest extends pageElements {
	public static String sRequestType;
	static Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	public static Date sTimeStamp = timestamp;

	public static void fQueueExecution(String sCan, String imsi) {
		try {

			Log.startTestCase("MQueue API exectuion for Request: * " + sCan + " * is started by Automation");
			RestAssured.baseURI = Driver.sBaseURI;
			RestAssured.proxy(Driver.ProxyAddress, Driver.ProxyPort);
			String data = getCanRequestPayload(sCan, imsi);
			Log.info("Api request URI is:                " + Driver.sBaseURI + Driver.floQueue);
			Log.info("Api request data is:              " + data);
			Response res = (Response) given().log().body().log().uri().body(data).when().post(Driver.floQueue);
			String sResult = res.prettyPrint();
			int sResponseCode = res.getStatusCode();
			if (sResponseCode == 200) {
				Log.pass("Api Response code is 200");
			} else
				Log.fail("Response code is invalid: [" + sResponseCode + "] Please verify it once.");

			if (res.contentType().equals("application/json; charset=utf-8")) {
				Log.pass("Response content type is application/json; charset=utf-8");
			} else
				Log.fail("Invalid response type displayed : " + res.contentType()
						+ " Expecting Application/Json please have a look at api call flo mqueue");

			long iResponseTime = res.time();
//			String sResponseType = res.getContentType();
			Log.info("Response time is ** " + iResponseTime);

			Log.info("Api response is " + sResult);
		} catch (Exception e) {
			e.printStackTrace();
			Log.info("Exception occures ***VPN might be down*** or please debug mQueue code");
		}

	}

	public static void fCanRequests(String sCan, String imsi) {
		try {
			Log.startTestCase(
					"MNO Provisioning for  " + sCan.toUpperCase() + "  API execution is Started by automation");
			RestAssured.baseURI = Driver.sBaseURI;
			RestAssured.proxy(Driver.ProxyAddress, Driver.ProxyPort);
			if (sCan.equals("canProvide")) {
				sRequestType = Driver.canProvideRequest;
			} else if (sCan.equals("canSuspend")) {
				sRequestType = Driver.canSuspendRequest;
			} else if (sCan.equals("canCease")) {
				sRequestType = Driver.canCeaseRequest;
			} else if (sCan.equals("canBounce")) {
				sRequestType = Driver.canBounceRequest;
			} else if (sCan.equals("canRestore")) {
				sRequestType = Driver.canRestoreRequest;
			}
			Log.info("Api Request URL is: " + RestAssured.baseURI + sRequestType);

			String sData = getCanRequestPayload(sCan, imsi);
			Log.info("Api Request data is :       " + sData);

			Response res = (Response) given().log().body().log().uri().body(sData).when().post(sRequestType);

			Log.info(
					"-------------------------------------------------------------**** Response Body Started ****-------------------------------------------------------------");
			JsonPath jsonres = utility.rawToJson(res);
			Log.pass("RequestId for API call   " + sRequestType.toUpperCase() + "*************** is:    "
					+ jsonres.get("requestID"));
			String sVerifyError = jsonres.get("IMSIs[0].status.status");
			if (sVerifyError.contains("ERR")) {
				Log.fail("API is showing error for request -------- " + sRequestType + " Error Message is : ----->"
						+ jsonres.get("IMSIs[0].status.errorMessage"));

			} else if (sVerifyError.contains("OK")) {
				Log.pass("For request ---------------[" + sRequestType
						+ "] Provisioning response output is --------------- " + jsonres.get("IMSIs[0].status.status"));
				Log.pass(" Error Code is ---------------  " + jsonres.get("IMSIs[0].status.errorCode"));
				Log.pass(" and Error message is ---------------  " + jsonres.get("IMSIs[0].status.errorMessage"));
			} else {
				Log.warn("Some error occured " + "For request   \r\n[" + sRequestType
						+ "] Provisioning response output is --------------- " + jsonres.get("IMSIs[0].status.status"));
			}
			int sResponseCode = res.getStatusCode();
			if (sResponseCode == 200) {
				Log.info("Response code is ************  200   ***************");
			} else
				Log.fail("Invalid response code: " + sResponseCode);

			String sResponseType = res.getContentType();
			String sResult = res.prettyPrint();
			if (sResponseType.equals("application/json; charset=utf-8")) {
				Log.info("Response content type is: " + sResponseType);
			} else
				Log.fail("Response content type is invalid " + sResponseType
						+ " please have a look at flo Sync Requests apis");
// 			String sApiResult = res.body("status.status", equalTo("OK"));
			long iResponseTime = res.time();
			Log.info("API Response time is ***********   " + iResponseTime);
//			JSONObject jsonObj = new JSONObject(res.asString());
			Log.info("Api response is " + sResult);

		} catch (Exception e) {
			e.printStackTrace();
			Log.info("Exception occures ***VPN might be down*** or please debug Sync Request code");
		}

	}

	public static void fProvRequests(String sRequest, String imsi) {
		try {
			Log.startTestCase("MNO Provision Requests execution is Started for operator: "
					+ Driver.sOperator.toUpperCase() + " and Api request : " + sRequest.toUpperCase()
					+ "  by automation using RestAssured tool");
			RestAssured.baseURI = Driver.sBaseURI;
			RestAssured.proxy(Driver.ProxyAddress, Driver.ProxyPort);

			if (sRequest.equals("provide")) {
				sRequestType = Driver.sProvideRequest;
			} else if (sRequest.equals("suspend")) {
				sRequestType = Driver.sSuspendRequest;
			} else if (sRequest.equals("cease")) {
				sRequestType = Driver.sCeaseRequest;
			} else if (sRequest.equals("bounce")) {
				sRequestType = Driver.sBounceRequest;
			} else if (sRequest.equals("restore")) {
				sRequestType = Driver.sRestoreRequest;
			}
			Log.info("API Request URI is:     " + RestAssured.baseURI + sRequestType);

			String sData = getCanRequestPayload(sRequest, imsi);
			Log.info("Api Request data is :/r/n" + sData);

			Response res = (Response) given().log().body().log().uri().body(sData).when().post(sRequestType).then()
					.assertThat().statusCode(200).and().log().body().and().body("requestID", notNullValue()).extract()
					.response();

			Log.info(
					"-------------------------------------------------------------**** Response Body Started ****-------------------------------------------------------------");
			JsonPath jsonres = utility.rawToJson(res);
			Log.pass("RequestId for API call ************** " + sRequest.toUpperCase() + "  is "
					+ jsonres.get("requestID"));
			String sVerifyError = jsonres.get("IMSIs[0].status.status");
			if (sVerifyError.contains("ERR")) {
				Log.fail("API is showing error for request -------- " + sRequestType + " that: ----->"
						+ jsonres.get("IMSIs[0].status.errorMessage"));

			}
			if (sVerifyError.contains("OK"))
				Log.pass("For request ---------------  [" + sRequestType.toUpperCase()
						+ "]  API response is --------------- " + jsonres.get("IMSIs[0].status.status"));
			Log.pass(" Error Code is ---------------  " + jsonres.get("IMSIs[0].status.errorCode"));
			String sErrorMessage = jsonres.get("IMSIs[0].status.errorMessage");
			if (sErrorMessage.isEmpty()) {
				Log.warn("Error Message is not Available");
			} else
				Log.pass(" and Error message is ---------------  " + jsonres.get("IMSIs[0].status.errorMessage"));
//			String reqId = jsonres.getString("requestID");

			String sResponseType = res.getContentType();
			String sResult = res.prettyPrint();
			if (sResponseType.equals("application/json; charset=utf-8")) {

				Log.info("Response content type is Application/Json");
			} else
				Log.fail("Response content type is invalid " + sResponseType
						+ " please take a look at api call flo Provision Requests");

			int sResponseCode = res.getStatusCode();
			if (sResponseCode == 200) {
				Log.pass("Response code is 200");
			} else
				Log.fail("Invalid response code: " + sResponseCode);
//			String sApiResult = res.body("status.status", equalTo("OK"));
			long iResponseTime = res.time();
			Log.info("Response time is " + iResponseTime);

//			JSONObject jsonObj = new JSONObject(res.asString());
			Log.info("Api response is " + sResult);
		} catch (Exception e) {

			Log.fatal("Api is showing error " + e.toString());
			Log.info("Exception occures ***VPN might be down*** or please debug Async Request code");
		}

	}

}